# Lab : Inspection de l'utilisation des ressources du pod


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Les métriques Kubernetes permettent d'obtenir des informations approfondies sur une grande variété de données concernant des applications Kubernetes. Nous pouvons utiliser ces métriques pour comprendre comment des ressources de calcul sont utilisées. Dans ce lab, nous aurons l'opportunité de perfectionner nos compétences en examinant les pods existants exécutés dans un cluster Kubernetes pour déterminer lesquels utilisent le plus de CPU.

Pour ce faire nous allons : 

- Installer le serveur de métriques Kubernetes

- Localisez le pod utilisant le processeur et écrivez son nom dans un fichier

# Contexte

Vous travaillez pour BeeHive, une entreprise qui fournit des envois réguliers d'abeilles à ses clients. L'entreprise est en train de construire une infrastructure basée sur Kubernetes pour certains de ses logiciels.

Il semble qu'un pod dans le cluster utilise plus de CPU que prévu. Recherchez dans le namespace beebox-mobile les pods avec le label **"app=auth"** et déterminez lequel de ces pods utilise le plus de CPU. Enregistrez le nom de ce pod dans un fichier pour référence future.

Le cluster n'a pas de **"Serveur de Metrics"* installé, nous devrons donc l'installer pour effectuer cette tâche.

>![Alt text](img/image.png)

**Credit : [here](https://labkeep-assets-production.s3.amazonaws.com/y8o8l5lu5bxhc9ixbqbatudcg0z5?response-content-disposition=inline%3B%20filename%3D%22devops-wb002_-_S04-LAB03_Discovering_Pod_Resource_Usage_With_Kubernetes_Metrics.png%22%3B%20filename%2A%3DUTF-8%27%27devops-wb002_-_S04-LAB03_Discovering_Pod_Resource_Usage_With_Kubernetes_Metrics.png&response-content-type=image%2Fpng&X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=ASIAVKPCGNLN623W67G6%2F20240522%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20240522T132824Z&X-Amz-Expires=14400&X-Amz-Security-Token=IQoJb3JpZ2luX2VjEOX%2F%2F%2F%2F%2F%2F%2F%2F%2F%2FwEaCXVzLWVhc3QtMSJHMEUCIFdfc0JDfL8pEFi4fYjc9pG8dBULLhbFROdqqi31jjywAiEAidXqGMv%2F%2BvkNBRqy2xfK0ph2INZ7RvxBm0kbxqjG5lMqgwQIXhADGgwzNjYwODM0Njc5OTUiDOI3VZDkorf8G6tkCSrgAwtHUS5T4HeXKuUIM0Z%2BVUX8DW0z4z9pnoj%2F3FUcm%2B5wWMY8mFRsCKDlPzjD8PfqbGVtWDlQqgJajmDhmCzbGYEt%2BOC5Eq%2Fi3OgGO8%2B9nu8OHLO18EbbkhSux7MZLZVlfXUY0To8UQ2rIVT%2BLpH9qAUsqarAldghjK15lCFPM6cHccwXO%2BF8Q6xNmObN3gad0UYQ4PjWbhuic4OVpgPhQi1LZbXDDjMgF0ygQM9b%2FvDCTUb40lx8TMLhB%2FS9L6KEPOjEu8WAA%2BQL7hwOCTMGwMeS1NjDPEgvmdvjsVal%2FcBwEz1U1k2PpIhVRo%2FwCCGXTckhhvypJPo5Lg9I2Lz6rhxXuhZLqlikYMHjKcHQYq%2FUrYfiBWRvPQbEE2hLswUZT%2FjnEJVNcGege8Ul4KfBLe0rNSKNhMR03TQfWYUgLWqRiqXPYyRHSuzG8fERVRXwdm8iebhY5jMytNUSdeZOKVbeXPUgGjRtfynzGjOhkgwh11XlfYvBO%2FIaWzoH4CIsas3IL7l2%2Fmwh4aPaMqe8%2BCetxiBQbYQQdMCOBIL1TsV9DgXtxhHYz38IcjzkY%2BIZHo34KyNvlOMUIiu1s7cU%2BvVDYQsm9ELBxtjUSTK%2Bt7zWf3u2s3g5Ic%2BzyhZBah4sqjDDyreyBjqlAadHeT%2FC02meuXrSOIIW5d4w8spgaxcSdqpBrecDy0FHOtGO0PBR7K2FCHAbVFkA5uryyHPRsH%2BkslOejuEghBIpJnPCs7pQQPucpL%2BaVzfO3UEoflZVmLdJ39PkcL15qQuJUGn0gbb6QUCb2fJViGgiOuFIOn6Sz%2BUSjsGu1ASMaWhLRB11EeJtaLdEM78PQnHIcytD9nEBhomUHhkvPV8RchORrg%3D%3D&X-Amz-SignedHeaders=host&X-Amz-Signature=7da81b8a9b5cedbef657fbb3a8c4af9d20ff1f0ef992618d70906b1e1f3349c3)


# Pratique 

## 1. Connectez-vous au serveur

```bash
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

## 2. Installation du serveur de métriques Kubernetes

```bash
kubectl apply -f https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-inspection-de-lutilisation-des-ressources-de-pod/-/raw/main/metrics-server-components.yml
```

>![Alt text](img/image-1.png)
*Installation de Metric Server effectuée*

Il est important de savoir que **"Metric Server"** peut prendre quelques minutes avant d'être pleinement opérationnel et de commencer à collecter des métriques sur les pods du cluster

## 3. Vérifiez que Metrics Server est opérationel :

Nous allons interroger l'API du serveur de métriques pour nous assurer qu'il est opérationnel

```bash
kubectl get --raw /apis/metrics.k8s.io/
```

>![Alt text](img/image-2.png)

Le serveur de metriques est bien opérationnel, car nous obtenons une reponse de l'API de ce dernier.

## 4. Localisation des pods en utilisant le processeur et transcrire l'information dans un fichier

Listons à présent les pods ainsi que les ressources utilisées par chacun d'eux

```bash
kubectl top pod -n beebox-mobile
```

>![Alt text](img/image-3.png)
*Liste des pods et de l'allocation en ressource*

Dans la liste de pod qui s'affiche, l'on observe  un tri par odre alphabetique par rapport au nom des ressources.

Dans le namespace **"beebox-mobile"**, déterminons quel pod portant le label **"app=auth"** utilise le plus de processeur :

```bash
kubectl top pod -n beebox-mobile --sort-by cpu --selector app=auth
```

>![Alt text](img/image-4.png)
*Liste des pods portant le label app=auth*

l'on obtiens une liste avec pour critère de tri décroissant, la mémoire CPU en cours d'utilisation.

Si l'on reçoit un message d'erreur indiquant que les métriques ne sont pas disponibles, attendre quelques minutes, puis réexécutez la commande.

## 5. Sauvegarder le nom des pods dans un fichier :

```bash
echo auth-proc > /home/$USER/cpu-pod-name.txt
```

>![Alt text](img/image-5.png)
*Sauvegarde du nom du pod*

